/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/08 14:17:48 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/08 15:08:20 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */
#include<unistd.h>

int	checkbase(char *base)
{
	int	a;
	int	b;

	a = 0;
	b = 0;
	while (base[a])
	{
		b = a + 1;
		while (base[b])
		{
			if (base[a] == base[b]
				|| base[a] == '-'
				|| base[a] == '+')
				return (1);
			b++;
		}
		a++;
	}
	return (0);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int	l;

	l = 0;
	if (checkbase(base) == 1)
		return ;
	while (base[l])
		l++;
	if (l <= 1)
		return ;
	if (nbr >= 0 && l > nbr)
	{
		write(1, &base[nbr], 1);
		return ;
	}
	if (nbr < 0)
	{
		nbr = -nbr;
		write(1, "-", 1);
		ft_putnbr_base(nbr, base);
	}
	else
	{
		ft_putnbr_base(nbr / 10, base);
		ft_putnbr_base(nbr % 10, base);
	}
}
